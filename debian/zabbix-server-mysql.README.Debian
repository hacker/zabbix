Installing the database
-----------------------

The Zabbix server needs an SQL database to run. As you chose to install
this zabbix-server-mysql package you apparently want to use a MySQL server
as a storage backend.

Set up a MySQL database server on any system - you are not forced to run it
on the same computer as the Zabbix server:

-> apt-get install mysql-server

Create a new database (let's call it "zabbix"):

-> mysql -p -e "CREATE DATABASE zabbix CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"

Create a MySQL user that has access rights to the database
(please use another password than 'SECRETPASSWORD'):

-> mysql -p -e "grant all on zabbix.* to 'zabbix'@'localhost' identified by 'SECRETPASSWORD'"

Create the database schema:

-> zcat /usr/share/zabbix-server-mysql/{schema,images,data}.sql.gz \
   | mysql -uzabbix -pSECRETPASSWORD zabbix

Then enter the database access credentials into
"/etc/zabbix/zabbix_server.conf".


See also:

  https://www.zabbix.com/documentation/6.0/manual/appendix/install/db_scripts
  https://www.zabbix.com/documentation/6.0/manual/appendix/install/db_charset_coll
