Removed "widgets/autocomplete.js" to address ZBX-17697 [1]:

  Error: cannot call methods on autocomplete prior to initialization; attempted to call method 'off'

(The error happens on attempt to add or edit host).

[1]: https://support.zabbix.com/browse/ZBX-17697
